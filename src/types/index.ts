export class ExpressError extends Error {
  status: number
  name: string
  message: string

  constructor(message: string) {
    super(message)

    Object.setPrototypeOf(this, ExpressError.prototype)
  }
}
