import dotenv from 'dotenv'

process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const envFound = dotenv.config()
if (!envFound) {
  throw new Error('No .env found !')
}

export default {
  host: process.env.HOST,
  port: parseInt(process.env.PORT, 10),
  dbname: process.env.DB_NAME,
  mongo: process.env.MONGODB_URI,
  maria: process.env.MARIADB_URI,
}
