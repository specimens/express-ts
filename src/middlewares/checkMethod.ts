import { Request, Response, NextFunction } from 'express'
import { ExpressError } from '../types'

const method: string[] = ['GET', 'SEARCH', 'POST', 'PUT', 'PATCH', 'DELETE']

export default (err: ExpressError, req: Request, res: Response, next: NextFunction) => {
  if (method.includes(req.method)) {
    next()
  } else {
    let err = new ExpressError(`You Cannot Use This Method On This Server (${req.method})`)
    err.status = 405
    err.name = 'MethodNotAllowed'

    throw err
  }
}