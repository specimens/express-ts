import { validateOrReject } from 'class-validator'
import UserService from './user'

export const userServices = new UserService(validateOrReject)
