import { Router, Request, Response, NextFunction } from 'express'
import { userServices } from '../services'
import { sendResponse } from '../middlewares'

const user = Router()

export default (router: Router) => {
  router.use('/user', user)

  user.get('/', async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await userServices.getUser()
      if (data.length > 0) {
        sendResponse(res, 200, `${data.length} Data Found`, data)
      } else {
        sendResponse(res, 404, 'No Data Found', data)
      }
    } catch (err) {
      next(err)
    }
  })

  user.post('/', async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await userServices.postUser(req.body)
      if (data.result.ok && data.insertedCount) {
        sendResponse(res, 201, 'A Data Has Been Inserted', data.result)
      } else {
        sendResponse(res, 400, 'Cannot Insert Data', data)
      }
    } catch (err) {
      next(err)
    }
  })
}
