import { Request, Response, NextFunction } from 'express'
import { ExpressError } from '../types'

const requireBody: string[] = ['SEARCH', 'POST', 'PUT', 'PATCH']

export default (req: Request, res: Response, next: NextFunction) => {
  if (requireBody.includes(req.method)) {
    if (Object.keys(req.body).length === 0) {
      let err = new ExpressError('No Data Passed, Nothing To Do')
      err.status = 400
      err.name = 'BadRequest'

      throw err
    } else {
      next()
    }
  } else {
    next()
  }
}