class Connect {
  private client: any
  private db: any

  constructor(client: any) {
    this.client = client
  }

  public async connectDB () {
    try {
      this.db = await this.client()
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  public async getDB () {
    try {
      if (this.db) {
        return this.db
      } else {
        throw Error('Database is not connected')
      }
    } catch (error) {
      console.log(error)
      throw error
    }
  }
}

export default Connect
