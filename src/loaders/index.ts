import app from './express'
import { mongoConnection } from '../loaders/mongo'
import dependency from './dependency'

export default async ({ port, host }: { port: number; host: string }) => {
  try {
    await mongoConnection.connectDB()
    console.log('MongoDB connection established')

    await dependency(mongoConnection)
    console.log('Dependency injected')

    app.listen(port, host)
    console.log('Express.ts API running on port %d in %s mode', port, process.env.NODE_ENV)

  } catch (error) {
    console.log(error)
    process.exit(1)
  }
}
