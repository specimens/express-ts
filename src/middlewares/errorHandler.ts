import { Request, Response, NextFunction } from 'express'
import { ExpressError } from '../types'

export default (err: ExpressError, req: Request, res: Response, next: NextFunction) => {
  console.log(err)

  const errorObject = Object.assign({
    status: 500,
    name: 'InternalServerError',
    message: 'Something Bad Happen When Processing Your Request'
  }, err)

  res.status(err.status || 500).json(errorObject)
}