import { MongoClient, Db } from 'mongodb'
import config from '../config'
import Connect from './connect'

const mongo = async (): Promise<Db> => {
  try {
    const client = await MongoClient.connect(config.mongo, { poolSize: 10, useNewUrlParser: true, useUnifiedTopology: true })
    const db = client.db(config.dbname)

    return db
  } catch (err) {
    console.log(err)
    throw err
  }
}

export const mongoConnection = new Connect(mongo)
