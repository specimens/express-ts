import { Router, Request, Response, NextFunction } from 'express'
import user from './user'
import { checkMethod, requireData } from '../middlewares'

export default () => {
  const router = Router()

  router.use([checkMethod, requireData])

  router.get('/', (req: Request, res: Response, next: NextFunction) => {
    res.send('Express.ts API running')
  })

  user(router)

  return router
}
