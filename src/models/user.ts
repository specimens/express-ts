import { IsString, IsEmail, IsOptional } from 'class-validator'

export interface IUser {
  name: string
  email: string
  password: string
  birth?: string
}

export class User {
  @IsString()
  name: string

  @IsEmail()
  email: string

  @IsString()
  password: string

  @IsString()
  @IsOptional()
  birth: string

  constructor(data: IUser) {
   this.name = data.name
   this.email = data.email
   this.password = data.password
   if (data.birth) { this.birth = data.birth }
  }
}
