import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import helmet from 'helmet'

import api from '../api'
import { errorHandler } from '../middlewares'

const app = express()

app.enable('trust proxy')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(cors())

app.use(helmet())

app.use('/api', api())

app.use((req, res) => {
  return res.status(404).json({
    status: 404,
    message: `Cannot find '${req.url}' in this server`
  })
})

app.use(errorHandler)

export default app
