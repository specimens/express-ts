import { IUser, User } from '../models/user'
import Connect from '../loaders/connect'

export default class UserService {
  private validator: (user: User) => object
  private collection: any

  constructor(validator: (user: User) => object) {
    this.validator = validator
  }

  public async setupDB (mongo: Connect) {
    try {
      let db = await mongo.getDB()
      this.collection = await db.collection('user')
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  public async getUser() {
    try {
      const res = await this.collection.find().toArray()
      return res
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  public async postUser(data: IUser) {
    try {
      const user = new User(data)
      await this.validator(user)
      const res = await this.collection.insertOne(user)
      return res
    } catch (error) {
      console.log(error)
      throw error
    }
  }
}