import * as service from '../services'
import Connect from './connect'

export default async (mongoConnection: Connect) => {
  await service.userServices.setupDB(mongoConnection)
}